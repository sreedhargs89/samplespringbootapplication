package hello;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.lang.reflect.Method;

import static org.springframework.web.bind.annotation.RequestMethod.POST;

@RestController
public class UserController {

    @Autowired
    private UserRepository userRepository;

    // With POST used API -- http://localhost:8080/addUser/sree/sree@gmail.com
    @RequestMapping(value = "/addUser/{name}/{email}", method = POST)
    public @ResponseBody String addUser(@PathVariable("name") String name, @PathVariable("email") String email){

        User user = new User();
        user.setName(name);
        user.setEmail(email);

        userRepository.save(user);
        return "Saved";
    }

    //With GET used API -- http://localhost:8080/all
    @RequestMapping(path="/all")
    public @ResponseBody Iterable<User> getAllUser(){
        return userRepository.findAll();
    }

}
