# SpringBoot Application

As Maven is a dependency manager, you can add all the project dependencies to it.

For now, all we need is Spring Boot. Copy and paste the code below into the `pom.xml` file

- The `<parent>` tag tells Maven to inherit the properties of `spring-boot-starter-parent`, such as port number, configs, etc. All these are provided by Spring.
- The `<dependencies>` tag contains all the project dependencies. For now, we only have one dependency, `spring-boot-starter-web`. In a complete application, there could be more, e.g. MySQL, socket, JSON library, etc.
- The `<build>` contains build plugins, such as `spring-maven-plugin`.

Add the @SpringBootApplication annotation to the class to make it a Spring Boot application. You can now run the application.

## Controller, the C in MVC

- `@RestController` annotation tells Spring that this class is a controller.
- `@RequestMapping(“/”)` annotation means that any request (`GET`, `POST`, `PUT`, etc.) to the root `URL(/)` will be handled by the `index()` method. The response is of type `String`.

Other variants of the `@RequestMapping` annotation are `@GetMapping`, `@PostMapping`, etc. for handling `GET` and `POST` requests, respectively.

## **Handling Different Request Types**

Spring Boot annotations for handling different HTTP request types:

- `@RequestMapping` — For handling any request type.
- `@GetMapping` — For a GET request.
- `@PostMapping` — For a POST request.
- `@PutMapping` — For a PUT request.
- `@PatchMappin`g — For a PATCH request.
- `@DeleteMapping` — For a DELETE request.

**To make a POST request, the method handling the request will be annotated like this:**

    @RequestMapping(value = "path", method = RequestMethod.POST)
    public returnType methodName(){...}

Or like this:

    @PostMapping("path")
    public returnType methodName(){...}

# **Path Variable and Request Parameters**

![](https://miro.medium.com/max/1124/1*EPjZh8Y6Wm9eAZAcF_wnQw.png)

Path variables are variables in the request URL and annotated with `@PathVariable`, e.g `id`.

    @GetMapping("/blog/{id}")
    public returnType methodName(@PathVariable String id)
    {
      ......
    }

Request parameters are key-value pairs in the request URL with the annotation `@RequestParam`, e.g. `param1` and `param2`.

    @GetMapping("/blog")
    public returnType methodName(@RequestParam String param1, @RequestParam String param2)
    {
      ......
    }

# **Request Body**

The POST, PUT, and DELETE requests can contain a payload known as “request body”. The payload contains the data that could be stored or updated. The payload is usually in JSON format.

![](https://miro.medium.com/max/2282/1*FCp-9Zua2ufrjgY4xAoqDw.png)

The annotation for the request body is `@RequestBody`. As the request body is a key-value pair, it will be wise to declare it as a `Map<String, String>`.

    @PostMapping("/blog")public Blog create(@RequestBody Map<String, String> body){...}

To extract the respective keys and their values:

    String id = body.get("id");String title = body.get("title");String content = body.get("content");

# Application Configuration - JPA Integration

To connect to Sprig boot application

- Add the required dependencies (e.g. MySQL library) to the `pom.xml` file. These dependencies are the Java libraries needed to connect to the database.
- Provide the database connection properties to the application. These properties include the database connection string, port, username, password, etc.
- Create a class that talks to the database. This class is commonly referred to as a “repository class”.

# **Adding MySQL Dependencies**

To connect the Spring Boot application to the MySQL database, `mysql-connector-java` library is required.

Similarly, Spring JPA requires the `spring-boot-starter-data-jpa` library. We will talk more about JPA later.

Copy and paste the code below into the `pom.xml` file, inside the dependencies tag.

    <?xml version="1.0" encoding="UTF-8"?>
    <dependency>
       <groupId>org.springframework.boot</groupId>
       <artifactId>spring-boot-starter-data-jpa</artifactId>
    </dependency>
    <dependency>
        <groupId>mysql</groupId>
        <artifactId>mysql-connector-java</artifactId>
    </dependency>

# **Setting Connection Properties**

To connect the Spring Boot application to the database, we need to provide the database URL, username, and password to the application.

To do this, simply create a file `application.properties` inside the `resources` folder and paste the code below into it:

    spring.datasource.url=jdbc:mysql://localhost:3306/restapi
    spring.datasource.username=root
    spring.datasource.password= **

- `spring.datasource.url`: Contains the MySQL connection string.
- `spring.datasource.username`: Is the MySQL username.
- `spring.datasource.password`: Is the MySQL password.

Make sure to change the above properties to reflect your MySQL settings.

# **Talking to the Database: Creating a Repository Class**

Now that we have the database ready, dependencies installed, and connection properties provided, the next thing is to create a class that talks to the database. This class is commonly referred to as a *repository*.

- First, we create an interface that extends `JpaRepository`. The `JpaRepository` gives us some functionality out of the box, such as fetching all records, a single record, saving, updating, deleting, etc.
- Add the annotation `@Repository` to the interface to tell Spring that this is a repository.

Formula for creating a repository

    package hello;
    import org.springframework.data.repository.CrudRepository;
    public interface UserRepository extends CrudRepository<User, Integer> {}

## Converting Blog to Entity (@Entity)

To tell spring that **User.java** is an entry, we need to add a @Entity annotation to the class.